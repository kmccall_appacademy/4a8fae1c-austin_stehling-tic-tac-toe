class HumanPlayer
  attr_reader :name, :board
  attr_accessor :mark

  def initialize(name)
    @name = name
  end

  def display(board)
    puts board.grid
  end

  def get_move
    puts "where would you like to place your move?"
    pos = gets.chomp.split.map(&:to_i)
    pos 
  end

end
