

class Board
  attr_reader :grid, :marks

  def initialize(grid = [])
    if grid.empty?
      @grid = Array.new(3) { Array.new(3) }
    else
      @grid = grid
    end
    @marks = [:X, :O]
  end

  def place_mark(pos, mark)
    grid[pos[0]][pos[1]] = mark
  end

  def [](pos)
    row, col = pos
    grid[row][col]
  end

  def []=(pos, value)
    row, col = pos
    grid[row][col] = value
  end

  def empty?(pos)
    grid[pos[0]][pos[1]].nil? ? true : false
  end

  def winner
    (grid + grid.transpose + diagnol).each do |win|
      return :X if win == [:X,:X,:X]
      return :O if win == [:O,:O,:O]
    end
    nil
  end

  def diagnol
    diag1 = [[0, 0], [1, 1], [2, 2]]
    diag2 = [[0, 2], [1, 1], [2, 0]]

    [diag1, diag2].map do |diag|
      diag.map {|row, col| grid[row][col]}
    end
  end

  def over?
    return true if grid.flatten.compact.size == 9 || winner
    return false if grid.flatten.compact.size == 0 || winner.nil?
  end


end
