require_relative 'board'
require_relative 'human_player'
require_relative 'computer_player'

class Game
  attr_reader :board, :player_one, :player_two, :current_player

  def initialize (player_one, player_two)
    @player_one, @player_two = player_one, player_two
    @current_player = player_one
    @board = Board.new
    player_one = :X
    player_two = :O
  end

  def play_turn
    board.place_mark(current_player.get_move, current_player.mark)
    switch_players!
  end

  def switch_players!
    @current_player == player_two ? @current_player = player_one : @current_player = player_two
  end


end
